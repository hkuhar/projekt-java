package com.projekt;

public class Ticket {
    private String ticketName;
    private String date;
    private int cost;
    private Artist artist;
    private ConcertLocation concertLocation;
    private int seatID;

    public Ticket(String ticketName, String date, int cost, Artist artist, ConcertLocation concertLocation,int seatID){
        this.ticketName=ticketName;
        this.date=date;
        this.cost=cost;
        this.artist=artist;
        this.concertLocation=concertLocation;
        this.seatID=seatID;
    }

    public String getTicketName(){
        return ticketName;
    }
    public void getInfo(){
        System.out.println("Ticket:"+ticketName+" date:"+date+" price:"+cost+" seat number:"+seatID);
    }
}
