package com.projekt;
import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String firstName;
    private String lastName;
    private String oib;
    private List<Ticket> tickets;

    public Customer(String firstName, String lastName, String oib ){
        this.firstName=firstName;
        this.lastName=lastName;
        this.oib=oib;
        this.tickets = new ArrayList<>();
    }

    public void getInfo() {
        System.out.println("Customer name:" + firstName + " " + lastName + "Customer OIB:" + oib);
    }
    public void BuyTicket(Ticket ticket){
        tickets.add(ticket);
    }

    public void RemoveTicket(Ticket ticket){
        tickets.remove(ticket);
        System.out.println("Removed ticket:"+ticket.getTicketName());
    }

    public void ShowTickets(){
        System.out.println("\nCustomer "+firstName+" "+lastName+"'s tickets are:");
        for (Ticket ticket : tickets) {
            System.out.println(ticket.getTicketName());
        }
    }
}
