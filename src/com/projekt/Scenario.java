package com.projekt;

public class Scenario {
    public void run(){
        /* Create objects: */
        var korisnik = new Customer("Ivan","Ivanic","1234567890");
        var billieEilish= new Artist("Billie Eilish");
        var location=new ConcertLocation("Zagreb","neka adresa u zagrebu");

        /*
        Ticket name formatting: "TIK{Artist initials}{day}{month}{3 digit number}"
        Declare tickets:
        */
        var karta1 = new Ticket("TIKBE216001","21.6.2020",15,billieEilish,location,1);
        var karta2 = new Ticket("TIKBE216002","21.6.2020",15,billieEilish,location,2);
        var karta3 = new Ticket("TIKBE216003","21.6.2020",15,billieEilish,location,3);
        var karta4 = new Ticket("TIKBE216004","21.6.2020",15,billieEilish,location,4);
        var karta5 = new Ticket("TIKBE216005","21.6.2020",15,billieEilish,location,5);
        var karta6 = new Ticket("TIKBE216006","21.6.2020",15,billieEilish,location,6);
        var karta7 = new Ticket("TIKBE216007","21.6.2020",15,billieEilish,location,7);
        var karta8 = new Ticket("TIKBE216008","21.6.2020",15,billieEilish,location,8);
        var karta9 = new Ticket("TIKBE216009","21.6.2020",15,billieEilish,location,9);
        var karta10 = new Ticket("TIKBE2160010","21.6.2020",15,billieEilish,location,10);

        karta1.getInfo();

        /* Customer buys ticket(s): */
        korisnik.BuyTicket(karta1);
        korisnik.BuyTicket(karta2);
        korisnik.ShowTickets();
        korisnik.RemoveTicket(karta1);
        korisnik.ShowTickets();
    }
}