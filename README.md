**Naslov**:Rezervacija karata za koncert
* [ ] Desktop ili web aplikacija za rezervaciju karata za koncert
* [x] 	Svaka rezervacija sadrži datum i vrijeme održavanja koncerta, lokaciju, cijenu, ID sjedala, izvođača (npr. Petar Grašo)
* [ ] 	Korisnik može unositi novi, čitati, promijeniti (editirati) i brisati (CRUD operacije) postojeći zapis
* [ ] 	Korisnik može kliknuti na svaki zapis (npr. button Detalji) iz liste gdje vidi detalje zapisa
* [ ] 	Potrebno imati bazu (pravu bazu ili datoteku koja će predstavljati bazu, npr. json datoteku)
